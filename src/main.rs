use dotenv;
use serde::Deserialize;
use std::env;
use regex::Regex;

type Result<T> = std::result::Result<T, std::boxed::Box<dyn std::error::Error + std::marker::Send + std::marker::Sync>>;

fn break_ice() -> &'static str {
    // Would be nice to either make configurable or even take
    // from news outlets or trending twitter topics.
    //const icebreakers: [&str; 5] = [
    //    "What do you think about Goedemorgen?",
    //    "What do you think about #Borat2?",
    //    "What do you think about #PortugueseGP?",
    //    "What do you think about Belgische?",
    //    "What do you think about Poland?",
    //];
    return "What's new?";
}

#[derive(Deserialize)]
#[derive(Debug)]
struct PandoraResponse {
    responses: Vec<String>,
    sessionid: u32
}

struct Pandora {
    // Not changed after construction:
    name: String,
    referer: String,
    url: String,
    botkey: String,
    clientname: String,
    sessionid: u32,

    // Mutable:
    response: String
}
impl Pandora {
    async fn new(client: &reqwest::Client, message: &str) -> Result<Pandora> {
        let referer = env::var("referer").unwrap();
        let botkey = env::var("pandora_botkey").unwrap();
        let clientname = env::var("pandora_clientname").unwrap();
        let url = env::var("pandora_url").unwrap();
        let name = env::var("pandora_name").unwrap();
        let params: [(&str, &str); 4] = [
            ("input", "xintro"),
            ("botkey", &botkey),
            ("clientname", &clientname),
            ("sessionid", "null")
        ];
        let http_response = client.post(&url)
            .form(&params)
            .header("Referer", &referer)
            .send()
            .await?
            .json::<PandoraResponse>()
            .await?;
        Ok(Pandora {
            name: name,
            referer: referer.to_owned(),
            url: url,
            sessionid: http_response.sessionid,
            botkey: botkey,
            clientname: clientname,
            response: message.to_owned()
        })
    }

    // Say 'message' to pandora, expect a response
    async fn say(mut self, client: &reqwest::Client, message: &str) -> Result<Pandora> {
        let pandorasayparams = [
            ("input", message),
            ("botkey", &self.botkey),
            ("clientname", &self.clientname),
            ("sessionid", &self.sessionid.to_string())
        ];
        let re = Regex::new("<[^\\s]*").unwrap();
        let pandorares = client.post(&self.url)
            .form(&pandorasayparams)
            .header("Referer", &self.referer)
            .send()
            .await?
            .json::<PandoraResponse>()
            .await?;
        match pandorares.responses.get(0) {
            Some(response) => {
                let s = re.replace_all(response, "");
                if s.starts_with("Image from") {
                    print!("Sus pandora response: {:?}", pandorares);
                }
                if s.starts_with("I can currently play these games") {
                    self.response = "I love playing games! What games do you like yourself?".to_owned();
                } else {
                    self.response = s.into_owned();
                }
                Ok(self)
            },
            None => {
                print!("Empty pandora response: {:?}", pandorares);
                self.response = break_ice().to_owned();
                Ok(self)
            }
        }
    }
}

pub async fn chatscript_say(client: &reqwest::Client, url: &str, message: &str) -> Result<String> {
    let params = [("user", "peter"), ("send", ""), ("message", message)];
    let chatscript = client.post(url)
        .form(&params)
        .send()
        .await?
        .text()
        .await?;
    if chatscript.chars().next().unwrap() == '[' {
        Ok(chatscript.trim_start_matches(|c : char| c != ']').get(2..).unwrap().to_owned())
    } else {
        Ok(chatscript)
    }
}

#[tokio::main]
pub async fn main() -> Result<()> {
    dotenv::dotenv().ok();

    let client = reqwest::Client::new();

    let mut pandora: Pandora = Pandora::new(&client, "Hello, there!").await?;

    let chatscript_name = env::var("chatscript_name").unwrap();
    let chatscript_url = env::var("chatscript_url").unwrap();

    let mut cs_message: String;
    let mut count = 0;
    loop {
        count += 1;
        cs_message = chatscript_say(&client, &chatscript_url, &pandora.response).await?;
        println!("{}: {}", chatscript_name, cs_message);

        pandora = pandora.say(&client, &cs_message).await?;
        println!("{}: {}", pandora.name, pandora.response);

        if count == 5000 {
            break;
        }
    }
   
    Ok(())
}
